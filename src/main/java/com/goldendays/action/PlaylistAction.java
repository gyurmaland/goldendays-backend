package com.goldendays.action;

import com.goldendays.converter.PlaylistConverter;
import com.goldendays.converter.SongConverter;
import com.goldendays.error.ResourceNotFoundException;
import com.goldendays.model.Playlist;
import com.goldendays.model.Song;
import com.goldendays.payload.*;
import com.goldendays.service.PlaylistService;
import com.goldendays.service.SongService;
import com.goldendays.util.ObjectMapperUtils;
import com.goldendays.util.SpotifyAuthUtil;
import com.goldendays.util.SpotifyPlaylistUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class PlaylistAction {

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private SongService songService;

    @Autowired
    private PlaylistConverter playlistConverter;

    @Autowired
    private SongConverter songConverter;

    @Autowired
    private SpotifyAuthUtil spotifyAuthUtil;

    @Value("${spotify.playlistStartUri}")
    private String playlistStartUri;

    private HttpEntity<String> entity;

    public PlaylistResponse findById(Long id) {
        PlaylistResponse playlistResponse;
        Playlist playlist = playlistService.findById(id);
        playlistResponse = ObjectMapperUtils.map(playlist, PlaylistResponse.class);
        playlistResponse.setSongsLength(playlistService.findById(id).getSongs().size());
        
        String uri = playlistStartUri.concat(playlist.getSpotifyId().concat("?fields=followers.total"));
        RestTemplate restTemplate = new RestTemplate();
        entity = spotifyAuthUtil.getEntity();
        SpotifyPlaylistFollowersResponse spotifyPlaylistFollowersResponse = restTemplate.exchange(uri, HttpMethod.GET, entity, SpotifyPlaylistFollowersResponse.class).getBody();

        Integer followerCount = (Integer) ((LinkedHashMap) spotifyPlaylistFollowersResponse.getFollowers()).get("total");
        playlistResponse.setFollowers(followerCount);

        return playlistResponse;
    }

    public void getPlaylistFromSpotifyAndSave(SpotifyPlaylistRequest spotifyPlaylistRequest) {

        String playlistId = SpotifyPlaylistUtil.extractSpotifyPlaylistLink(spotifyPlaylistRequest.getSpotifyPlaylistLink());
        String uri = playlistStartUri.concat(playlistId);

        RestTemplate restTemplate = new RestTemplate();

        entity = spotifyAuthUtil.getEntity();

        SpotifyPlaylistResponse spotifyPlaylistResponse = restTemplate.exchange(uri, HttpMethod.GET, entity, SpotifyPlaylistResponse.class).getBody();
        SpotifyPagingPlaylistResponse spotifyPagingPlaylistResponse = null;

        ArrayList<Song> songList = new ArrayList<>();

        Map<String, Boolean> isSongDeletedFromPlaylistMap = new HashMap<>();

        List<Song> allSongsInDb = songService.findByPlaylists_SpotifyId(spotifyPlaylistResponse != null ? spotifyPlaylistResponse.getId() : null);

        for (Song dbSong : allSongsInDb) {
            isSongDeletedFromPlaylistMap.put(dbSong.getSpotifyId(), true);
        }

        LinkedHashMap linkedHashMap = (LinkedHashMap) spotifyPlaylistResponse.getTracks();
        String nextLink = "firstloop";
        int index = 0;
        while (Objects.nonNull(nextLink)) {
            ArrayList playlistItems = null;
            if (StringUtils.endsWith(nextLink, "limit=100")) {
                spotifyPagingPlaylistResponse = restTemplate.exchange(nextLink, HttpMethod.GET, entity, SpotifyPagingPlaylistResponse.class)
                        .getBody();
                playlistItems = (ArrayList) spotifyPagingPlaylistResponse.getItems();
            } else {
                playlistItems = (ArrayList) linkedHashMap.get("items");
            }

            for (Object playlistItem : playlistItems) {
                SpotifySongResponse spotifySongResponse = new SpotifySongResponse();
                LinkedHashMap linkedHashMap1 = (LinkedHashMap) playlistItem;
                LinkedHashMap linkedHashMap2 = (LinkedHashMap) linkedHashMap1.get("track");
                LinkedHashMap linkedHashMap3 = (LinkedHashMap) linkedHashMap2.get("album");
                ArrayList arrayList1 = (ArrayList) linkedHashMap3.get("artists");
                LinkedHashMap linkedHashMap4 = (LinkedHashMap) arrayList1.get(0);

                spotifySongResponse.setName((String) linkedHashMap2.get("name"));
                spotifySongResponse.setSpotifyId((String) linkedHashMap2.get("id"));
                spotifySongResponse.setArtist((String) linkedHashMap4.get("name"));

                Song song = songConverter.convert(spotifySongResponse);
                List<Song> songsInDb = songService.findByPlaylists_SpotifyIdAndSpotifyId(spotifyPlaylistResponse.getId(), song.getSpotifyId());
                if (songsInDb.isEmpty()) {
                    song.setSpotifyPosition(index);
                    songList.add(song);
                } else {
                    songService.updateSpotifyPosition(songService.findBySpotifyId(song.getSpotifyId()).getId(), index);
                }
                if (isSongDeletedFromPlaylistMap.containsKey(song.getSpotifyId())) {
                    isSongDeletedFromPlaylistMap.put(song.getSpotifyId(), false);
                }

                index = index + 1;
            }
            if (Objects.nonNull(spotifyPagingPlaylistResponse)) {
                if (Objects.isNull(spotifyPagingPlaylistResponse.getNext())) {
                    nextLink = null;
                } else {
                    nextLink = (String) spotifyPagingPlaylistResponse.getNext();
                }
            } else {
                nextLink = (String) linkedHashMap.get("next");
            }
        }

        for (Map.Entry<String, Boolean> song : isSongDeletedFromPlaylistMap.entrySet()) {
            if (song.getValue()) {
                songService.deleteBySpotifyId(song.getKey());
            }
        }

        Playlist playlist = playlistService.findBySpotifyId(spotifyPlaylistResponse.getId());

        if (Objects.isNull(playlist)) {
            playlist = playlistConverter.convert(spotifyPlaylistResponse);
            playlist.setSongs(songList);
            Playlist finalPlaylist = playlistService.savePlaylist(playlist);
            songService.saveAll(songList);
            saveSongsToPlaylist(songList, finalPlaylist);
        } else {
            Playlist editPlaylist = playlistConverter.convert(spotifyPlaylistResponse);
            playlist.setSongs(songList);

            songService.saveAll(songList);
            playlistService.savePlaylist(playlist);

            Playlist finalPlaylist = playlistService.findById(playlist.getId());

            saveSongsToPlaylist(songList, finalPlaylist);
            playlistService
                    .updatePlaylistById(editPlaylist.getName(), editPlaylist.getLink(), editPlaylist.getImageUrl(), editPlaylist.getFollowers(),
                            playlist.getId());
        }
    }

    private void saveSongsToPlaylist(ArrayList<Song> songList, Playlist finalPlaylist) {
        List<Song> songList1 = new CopyOnWriteArrayList<>(songList);

        for (Song song : songList1) {
            Song song1 = songService.findById(song.getId()).orElseThrow(() -> new ResourceNotFoundException("Song", "id", song.getId()));
            List<Playlist> finalPlayList2 = new ArrayList<>();
            finalPlayList2.add(finalPlaylist);
            song1.setPlaylists(finalPlayList2);
            songService.save(song1);
        }
    }
}
