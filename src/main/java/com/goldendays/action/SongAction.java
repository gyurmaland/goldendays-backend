package com.goldendays.action;

import com.goldendays.model.Song;
import com.goldendays.payload.SongListRequest;
import com.goldendays.payload.SongResponse;
import com.goldendays.service.SongService;
import com.goldendays.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SongAction {

    @Autowired
    private SongService songService;

    public List<SongResponse> findTop10SongsByVotes(Long playlistId) {
        return ObjectMapperUtils.mapAll(songService.findTop10SongsByVotes(playlistId, PageRequest.of(0, 10)).getContent(), SongResponse.class);
    }

    public Page<SongResponse> searchSongs(SongListRequest songListRequest) {
        PageRequest pageRequest = PageRequest.of(songListRequest.getPage(), songListRequest.getPageSize());
        Page<Song> songPage = songService.searchSongs(songListRequest, pageRequest);
        List<SongResponse> songResponseList = ObjectMapperUtils.mapAll(songPage.getContent(), SongResponse.class);

        return new PageImpl<>(songResponseList, PageRequest.of(songListRequest.getPage(), songListRequest.getPageSize()), songPage.getTotalElements());
    }
}
