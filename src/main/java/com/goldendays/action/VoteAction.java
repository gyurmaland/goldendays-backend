package com.goldendays.action;

import com.goldendays.email.EmailService;
import com.goldendays.email.Mail;
import com.goldendays.error.AppException;
import com.goldendays.error.ResourceNotFoundException;
import com.goldendays.error.UnauthenticatedException;
import com.goldendays.model.AppUser;
import com.goldendays.model.Playlist;
import com.goldendays.model.Song;
import com.goldendays.model.Vote;
import com.goldendays.payload.*;
import com.goldendays.service.AppUserService;
import com.goldendays.service.PlaylistService;
import com.goldendays.service.SongService;
import com.goldendays.service.VoteService;
import com.goldendays.util.KeyGeneration;
import com.goldendays.util.ObjectMapperUtils;
import com.goldendays.util.SpotifyAuthUtil;
import freemarker.template.TemplateException;
import freemarker.template.utility.StringUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

@Component
public class VoteAction {

    @Autowired
    private EmailService emailService;

    @Autowired
    private VoteService voteService;

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private SongService songService;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private SpotifyAuthUtil spotifyAuthUtil;

    @Value("${facebook.graph.me.id}")
    private String facebookGraphMeId;

    @Value("${spotify.playlistStartUri}")
    private String spotifyPlaylistUri;

    private Map<Long, Integer> songIdAndCount = new HashMap<>();
    private Map<Integer, Integer> placeAndCount = new HashMap<>();
    private List<Vote> voteList = new ArrayList<>();

    public void castVotes(VoteListRequest voteListRequest) {
        songIdAndCount = new HashMap<>();
        placeAndCount = new HashMap<>();
        voteList = new ArrayList<>();

        String activationKey = KeyGeneration.generateKey();
        AppUser appUser = appUserService.findByEmail(voteListRequest.getUserEmail());
        if (Objects.isNull(appUser)) {
            appUser = new AppUser();
            appUser.setEmail(voteListRequest.getUserEmail());
            appUserService.saveAppUser(appUser);
        }

        checkAlreadyVotedAndSize(voteListRequest.getVoteRequestList(), appUser.getVotes(), voteListRequest.getPlaylistId());

        for (VoteRequest voteRequest : voteListRequest.getVoteRequestList()) {
            voteCountAndStuff(voteRequest);

            Vote vote = new Vote();
            vote.setAppUser(appUser);
            vote.setPlace(voteRequest.getPlace());
            vote.setSong(songService.findById(voteRequest.getSongId())
                    .orElseThrow(() -> new ResourceNotFoundException("Song", "id", voteRequest.getSongId())));
            vote.setPlaylist(playlistService.findById(voteListRequest.getPlaylistId()));
            vote.setActivationCode(activationKey);
            vote.setEnabled(false);
            voteList.add(vote);
        }

        checkDuplicateSongsAndVotes(songIdAndCount, placeAndCount);

        voteService.castVotes(voteList);

        Mail mail = new Mail();
        mail.setFrom("goldendaysofedm@goldendaysofedm.com");
        mail.setTo(voteListRequest.getUserEmail());
        mail.setSubject("Confirm Vote");

        Map<String, String> model = new HashMap<>();
        model.put("link", "https://www.goldendaysofedm.com/vote/" + activationKey);
        mail.setModel(model);

        try {
            emailService.sendSimpleMessage(mail);
        } catch (MessagingException | IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public void castVotesWithFacebookAcc(FacebookVoteListRequest facebookVoteListRequest) {
        songIdAndCount = new HashMap<>();
        placeAndCount = new HashMap<>();
        voteList = new ArrayList<>();

        AppUser appUser = appUserService.findByFacebookId(facebookVoteListRequest.getFacebookUserId());
        if (Objects.isNull(appUser)) {
            appUser = new AppUser();
            appUser.setEmail(facebookVoteListRequest.getUserEmail());
            appUser.setFacebookId(facebookVoteListRequest.getFacebookUserId());
            appUserService.saveAppUser(appUser);
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        String rawJson = restTemplate.postForObject(facebookGraphMeId.concat(facebookVoteListRequest.getFacebookAccessToken()), request, String.class);

        assert rawJson != null;
        JSONObject root = new JSONObject(rawJson);

        String facebookId = root.getString("id");

        if (!facebookId.equals(facebookVoteListRequest.getFacebookUserId())) {
            throw new UnauthenticatedException("Facebook authentication went wrong");
        }

        checkAlreadyVotedAndSize(facebookVoteListRequest.getVoteRequestList(), appUser.getVotes(), facebookVoteListRequest.getPlaylistId());

        for (VoteRequest voteRequest : facebookVoteListRequest.getVoteRequestList()) {
            voteCountAndStuff(voteRequest);

            Song song = songService.findById(voteRequest.getSongId())
                    .orElseThrow(() -> new ResourceNotFoundException("Song", "id", voteRequest.getSongId()));

            Vote vote = new Vote();
            vote.setAppUser(appUser);
            vote.setPlace(voteRequest.getPlace());
            vote.setSong(song);
            vote.setPlaylist(playlistService.findById(facebookVoteListRequest.getPlaylistId()));
            vote.setEnabled(true);
            voteList.add(vote);
        }

        checkDuplicateSongsAndVotes(songIdAndCount, placeAndCount);

        setSongWeights(voteList);

        List<Song> songListOrderedByWeight = songService.findAllByPlaylists_IdOrderByWeightAscSpotifyPositionAsc(facebookVoteListRequest.getPlaylistId());
        List<Song> songListOrderedBySpotifyPosition = songService.findAllByPlaylists_IdOrderBySpotifyPositionAsc(facebookVoteListRequest.getPlaylistId());
        Playlist playlist = playlistService.findById(facebookVoteListRequest.getPlaylistId());

        orderSpotifyTracks(restTemplate, songListOrderedByWeight, songListOrderedBySpotifyPosition, playlist);

        voteService.castVotes(voteList);
    }

    public void deCastVotesByUserEmailAndPlaylistId(VoteListDecastRequest voteListDecastRequest) {
        voteService.deCastVotesByUserEmailAndPlaylistId(voteListDecastRequest.getUserEmail(), voteListDecastRequest.getPlaylistId());
    }

    public int countAllVotesByPlaylistId(Long playlistId) {
        return voteService.countAllVotesByPlaylistId(playlistId);
    }

    public void enableVotes(String activationCode) {
        List<Vote> voteList = voteService.findVotesByActivationCode(activationCode);
        if (voteList.isEmpty()) {
            throw new AppException("Activation code not found!");
        }

        setSongWeights(voteList);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<Song> songListOrderedByWeight = songService.findAllByPlaylists_IdOrderByWeightAscSpotifyPositionAsc(voteList.get(0).getPlaylist().getId());
        List<Song> songListOrderedBySpotifyPosition = songService.findAllByPlaylists_IdOrderBySpotifyPositionAsc(voteList.get(0).getPlaylist().getId());
        Playlist playlist = playlistService.findById(voteList.get(0).getPlaylist().getId());

        orderSpotifyTracks(restTemplate, songListOrderedByWeight, songListOrderedBySpotifyPosition, playlist);

        voteService.enableVotesWithEmail(activationCode);
    }

    public List<VoteResponse> findVotesByUserEmailAndPlaylistId(String userEmail, Long playlistId) {
        return ObjectMapperUtils.mapAll(voteService.findVotesByAppUserEmailAndPlaylistId(userEmail, playlistId), VoteResponse.class);
    }

    public boolean isUserAlreadyVotedOnThisPlaylist(String userEmail, Long playlistId) {
        return !voteService.findVotesByAppUserEmailAndPlaylistId(userEmail, playlistId).isEmpty();
    }

    public List<VoteResponse> getVotesByAppUserEmail(String email) {
        return ObjectMapperUtils.mapAll(voteService.findVotesByAppUserEmail(email), VoteResponse.class);
    }

    private void voteCountAndStuff(VoteRequest voteRequest) {
        Integer songIdCount = songIdAndCount.get(voteRequest.getSongId());
        if (songIdCount == null) {
            songIdAndCount.put(voteRequest.getSongId(), 1);
        } else {
            songIdAndCount.put(voteRequest.getSongId(), ++songIdCount);
        }

        Integer placeCount = placeAndCount.get(voteRequest.getPlace());
        if (placeCount == null) {
            placeAndCount.put(voteRequest.getPlace(), 1);
        } else {
            placeAndCount.put(voteRequest.getPlace(), ++placeCount);
        }
    }

    private void checkAlreadyVotedAndSize(List<VoteRequest> voteRequestList, List<Vote> voteList, Long playlistId) {
        if (voteRequestList.size() != 5) {
            throw new AppException("There must be exactly 5 songs!");
        }

        if (Objects.nonNull(voteList)) {
            for (Vote userVote : voteList) {
                if (Objects.equals(userVote.getPlaylist().getId(), playlistId)) {
                    throw new AppException("User already voted on this playlist!");
                }
            }
        }
    }

    private void checkDuplicateSongsAndVotes(Map<Long, Integer> songIdAndCount, Map<Integer, Integer> placeAndCount) {
        Set<Map.Entry<Long, Integer>> songIdEntrySet = songIdAndCount.entrySet();
        for (Map.Entry<Long, Integer> entry : songIdEntrySet) {
            if (entry.getValue() > 1) {
                throw new AppException("Duplicate songs!");
            }
        }

        Set<Map.Entry<Integer, Integer>> placeEntrySet = placeAndCount.entrySet();
        for (Map.Entry<Integer, Integer> entry : placeEntrySet) {
            if (entry.getValue() > 1) {
                throw new AppException("Duplicate vote place!");
            }
        }
    }

    private void setSongWeights(List<Vote> voteList) {
        for (Vote vote : voteList) {
            Song song = songService.findById(vote.getSong().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Song", "id", vote.getSong().getId()));
            song.setWeight(song.getWeight() + getSongWeightFromVotePlace(vote.getPlace()));
            songService.save(song);
        }
    }

    private int getSongWeightFromVotePlace(int votePlace) {
        switch (votePlace) {
            case 1:
                return 5;
            case 2:
                return 4;
            case 3:
                return 3;
            case 4:
                return 2;
            case 5:
                return 1;
            default:
                return 0;
        }
    }

    private void orderSpotifyTracks(RestTemplate restTemplate, List<Song> songListOrderedByWeight, List<Song> songListOrderedBySpotifyPosition, Playlist playlist) {
        for (int i = 0; i < songListOrderedByWeight.size(); i++) {
            if (songListOrderedByWeight.get(i).getWeight() != 0) {
                HttpHeaders httpHeaders = spotifyAuthUtil.getHeaders();
                String jsonp = "{\"range_start\":".concat(StringUtil.tryToString(songListOrderedByWeight.get(i).getSpotifyPosition())).concat(",\"insert_before\":".concat(StringUtil.tryToString(0).concat("}")));
                HttpEntity<String> requestEntity = new HttpEntity<>(jsonp, httpHeaders);
                String url = spotifyPlaylistUri.concat(playlist.getSpotifyId()).concat("/tracks");
                restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Void.class);

                Song newFirstsong = songListOrderedByWeight.get(i);
                songListOrderedBySpotifyPosition.remove(newFirstsong);
                songListOrderedBySpotifyPosition.add(0, newFirstsong);
                for (int j = 0; j < songListOrderedBySpotifyPosition.size(); j++) {
                    Song songWithNewSpotifyPosition = songListOrderedBySpotifyPosition.get(j);
                    songWithNewSpotifyPosition.setSpotifyPosition(j);
                    songService.save(songWithNewSpotifyPosition);
                }
            }
        }
    }
}
