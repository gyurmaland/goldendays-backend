package com.goldendays.payload;

public class SpotifyPagingPlaylistResponse {

    private Object items;

    private Object next;

    public Object getItems() {
        return items;
    }

    public void setItems(Object items) {
        this.items = items;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }
}
