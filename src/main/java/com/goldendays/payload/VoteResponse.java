package com.goldendays.payload;

public class VoteResponse {

    private SongResponse song;

    private int place;

    private boolean enabled;

    public SongResponse getSong() {
        return song;
    }

    public void setSong(SongResponse song) {
        this.song = song;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
