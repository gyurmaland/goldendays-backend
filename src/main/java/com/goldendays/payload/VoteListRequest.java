package com.goldendays.payload;

import javax.validation.constraints.Email;
import java.util.List;

public class VoteListRequest {

    @Email
    private String userEmail;

    private Long playlistId;

    private List<VoteRequest> voteRequestList;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Long playlistId) {
        this.playlistId = playlistId;
    }

    public List<VoteRequest> getVoteRequestList() {
        return voteRequestList;
    }

    public void setVoteRequestList(List<VoteRequest> voteRequestList) {
        this.voteRequestList = voteRequestList;
    }
}
