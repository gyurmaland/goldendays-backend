package com.goldendays.payload;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class VoteRequest {

    private Long songId;

    @Min(1)
    @Max(5)
    private Integer place;

    public Long getSongId() {
        return songId;
    }

    public void setSongId(Long songId) {
        this.songId = songId;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }
}
