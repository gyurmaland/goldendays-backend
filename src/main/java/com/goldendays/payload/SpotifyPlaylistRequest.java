package com.goldendays.payload;

public class SpotifyPlaylistRequest {

    private String spotifyPlaylistLink;

    public String getSpotifyPlaylistLink() {
        return spotifyPlaylistLink;
    }

    public void setSpotifyPlaylistLink(String spotifyPlaylistLink) {
        this.spotifyPlaylistLink = spotifyPlaylistLink;
    }
}
