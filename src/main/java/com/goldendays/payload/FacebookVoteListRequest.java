package com.goldendays.payload;

import javax.validation.constraints.Email;
import java.util.List;

public class FacebookVoteListRequest {

    @Email
    private String userEmail;

    private String facebookUserId;

    private String facebookAccessToken;

    private Long playlistId;

    private List<VoteRequest> voteRequestList;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getFacebookAccessToken() {
        return facebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    public Long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Long playlistId) {
        this.playlistId = playlistId;
    }

    public List<VoteRequest> getVoteRequestList() {
        return voteRequestList;
    }

    public void setVoteRequestList(List<VoteRequest> voteRequestList) {
        this.voteRequestList = voteRequestList;
    }
}
