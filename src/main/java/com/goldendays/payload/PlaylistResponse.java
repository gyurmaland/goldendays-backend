package com.goldendays.payload;

public class PlaylistResponse {

    private String name;

    private int followers;

    private String imageUrl;

    private String link;

    private int songsLength;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getSongsLength() {
        return songsLength;
    }

    public void setSongsLength(int songsLength) {
        this.songsLength = songsLength;
    }
}
