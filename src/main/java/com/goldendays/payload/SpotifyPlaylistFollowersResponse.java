package com.goldendays.payload;

public class SpotifyPlaylistFollowersResponse {

    private Object followers;

    public Object getFollowers() {
        return followers;
    }

    public void setFollowers(Object followers) {
        this.followers = followers;
    }
}
