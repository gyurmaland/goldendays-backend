package com.goldendays.payload;

import javax.validation.constraints.Email;
import java.util.List;

public class VoteSubmitRequest {

    @Email
    private String userEmail;

    private List<Long> songIds;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<Long> getSongIds() {
        return songIds;
    }

    public void setSongIds(List<Long> songIds) {
        this.songIds = songIds;
    }
}
