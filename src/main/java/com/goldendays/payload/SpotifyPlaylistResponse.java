package com.goldendays.payload;

import java.util.List;

public class SpotifyPlaylistResponse {

    private String id;

    private String name;

    private Object external_urls;

    private List<Object> images;

    private Object followers;

    private Object tracks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getExternal_urls() {
        return external_urls;
    }

    public void setExternal_urls(Object external_urls) {
        this.external_urls = external_urls;
    }

    public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }

    public Object getFollowers() {
        return followers;
    }

    public void setFollowers(Object followers) {
        this.followers = followers;
    }

    public Object getTracks() {
        return tracks;
    }

    public void setTracks(Object tracks) {
        this.tracks = tracks;
    }
}
