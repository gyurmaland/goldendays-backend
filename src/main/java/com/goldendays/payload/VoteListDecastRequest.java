package com.goldendays.payload;

import javax.validation.constraints.Email;

public class VoteListDecastRequest {

    @Email
    private String userEmail;

    private Long playlistId;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Long playlistId) {
        this.playlistId = playlistId;
    }
}
