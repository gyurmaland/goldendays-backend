package com.goldendays.repository;

import com.goldendays.model.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface PlaylistRepository extends JpaRepository<Playlist, Long> {

    Playlist findBySpotifyId(String spotifyId);

    @Modifying
    @Transactional
    @Query("update Playlist p set p.name = ?1, p.link = ?2, p.imageUrl = ?3, p.followers = ?4 where p.id = ?5")
    void updatePlaylistById(String name, String link, String imageUrl, int followers, Long id);
}
