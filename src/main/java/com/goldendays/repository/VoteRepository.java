package com.goldendays.repository;

import com.goldendays.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Long> {

    @Modifying
    @Transactional
    @Query("update Vote v set v.enabled = true, v.activationCode = '' where v.activationCode =:activationCode")
    void enableVotesWithEmail(@Param("activationCode") String activationCode);

    @Modifying
    @Transactional
    @Query("delete from Vote v where v.playlist.id = :playlistId and v.appUser.email = :userEmail and v.enabled = false")
    void deleteVoteByUserEmailAndPlaylistId(@Param("userEmail") String userEmail, @Param("playlistId") Long playlistId);

    @Query("select count(distinct v.appUser.id) from Vote v where v.playlist.id = :playlistId and v.enabled = true")
    int countAllVotesByPlaylistId(Long playlistId);

    List<Vote> findVotesByAppUserId(Long userId);

    List<Vote> findVotesByAppUserEmailAndPlaylistId(String appUserEmail, Long playlistId);

    List<Vote> findVotesByAppUserEmail(String email);

    List<Vote> findVotesByActivationCode(String activationCode);
}
