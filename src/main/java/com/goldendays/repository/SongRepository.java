package com.goldendays.repository;

import com.goldendays.model.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface SongRepository extends JpaRepository<Song, Long> {

    Song findBySpotifyId(String spotifyId);

    List<Song> findAllByPlaylists_IdOrderBySpotifyPositionAsc(Long playlistId);

    List<Song> findAllByPlaylists_IdOrderByWeightAscSpotifyPositionAsc(Long playlistId);

    List<Song> findByPlaylists_SpotifyId(String playlistSpotifyId);

    List<Song> findByPlaylists_SpotifyIdAndSpotifyId(String playlistSpotifyId, String songSpotifyId);

    Song findByPlaylists_IdAndAndSpotifyPosition(Long playlistId, Integer spotifyPosition);

    Page<Song> findAllByPlaylists_IdAndWeightNotOrderByWeightDesc(Long playlistId, int notZero, Pageable pageable);

    @Modifying
    @Transactional
    @Query("delete from Song s where s.spotifyId = ?1")
    void deleteBySpotifyId(String spotifyId);

    @Query("select s from Song s join s.playlists p " +
            "where (:playlistId is null or p.id = :playlistId) " +
            "and (:playlistName = '' or lower(p.name) like concat('%', lower(:playlistName), '%'))" +
            "and (:searchStr = '' or lower(s.artist) like concat('%', lower(:searchStr), '%'))" +
            "or (:searchStr = '' or lower(s.title) like concat('%', lower(:searchStr), '%')) " +
            "or (:searchStr = '' or concat(lower(s.artist), ' ', lower(s.title)) like concat('%', lower(:searchStr), '%')) order by s.spotifyPosition asc")
    Page<Song> searchSongs(@Param("playlistId") Long playlistId,
                           @Param("playlistName") String playlistName,
                           @Param("searchStr") String searchStr,
                           Pageable pageable);

    @Modifying
    @Transactional
    @Query("update Song s set s.weight = s.weight + ?1 where s.id = ?2")
    void updateSongWeight(int weight, Long id);

    @Modifying
    @Transactional
    @Query("update Song s set s.spotifyPosition = ?1 where s.id = ?2")
    void updateSpotifyPosition(int weight, Long id);
}
