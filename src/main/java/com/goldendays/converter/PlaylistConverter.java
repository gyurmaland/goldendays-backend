package com.goldendays.converter;

import com.goldendays.model.Playlist;
import com.goldendays.payload.SpotifyPlaylistResponse;
import com.goldendays.util.SpotifyPlaylistUtil;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;

@Component
public class PlaylistConverter {

    public Playlist convert(SpotifyPlaylistResponse spotifyPlaylistResponse) {
        Playlist playlist = new Playlist();
        convert(playlist, spotifyPlaylistResponse);
        return playlist;
    }

    private void convert(Playlist destination, SpotifyPlaylistResponse source) {
        destination.setName(source.getName());
        destination.setSpotifyId(source.getId());
        destination.setLink(SpotifyPlaylistUtil.getValuesFromLinkedHashMap((LinkedHashMap) source.getExternal_urls(), "spotify=", "}"));
        destination.setImageUrl((SpotifyPlaylistUtil.getValuesFromLinkedHashMap((LinkedHashMap) source.getImages().get(0), "url=", ",")));
        destination.setFollowers(Integer.parseInt(SpotifyPlaylistUtil.getValuesFromLinkedHashMap((LinkedHashMap) source.getFollowers(), "total=", "}")));
    }
}
