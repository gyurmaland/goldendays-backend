package com.goldendays.converter;

import com.goldendays.model.Song;
import com.goldendays.payload.SpotifySongResponse;
import org.springframework.stereotype.Component;

@Component
public class SongConverter {

    public Song convert(SpotifySongResponse spotifySongResponse) {
        Song song = new Song();
        convert(song, spotifySongResponse);
        return song;
    }

    private void convert(Song destination, SpotifySongResponse source) {
        destination.setSpotifyId(source.getSpotifyId());
        destination.setArtist(source.getArtist());
        destination.setTitle(source.getName());
    }
}
