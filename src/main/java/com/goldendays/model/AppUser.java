package com.goldendays.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class AppUser extends BaseModel {

    private String email;

    private String facebookId;

    @OneToMany(mappedBy = "appUser", fetch = FetchType.LAZY)
    private List<Vote> votes;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
