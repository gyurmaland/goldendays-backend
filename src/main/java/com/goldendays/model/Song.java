package com.goldendays.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Song extends BaseModel {

    private String spotifyId;

    private String artist;

    private String title;

    @Column(columnDefinition = "integer default 0")
    private int weight = 0;

    private Integer spotifyPosition;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Song_Playlist",
            joinColumns = {@JoinColumn(name = "song_id")},
            inverseJoinColumns = {@JoinColumn(name = "playlist_id")}
    )
    private List<Playlist> playlists;

    @OneToMany(mappedBy = "song", fetch = FetchType.LAZY)
    private List<Vote> votes;

    public String getSpotifyId() {
        return spotifyId;
    }

    public void setSpotifyId(String spotifyId) {
        this.spotifyId = spotifyId;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Integer getSpotifyPosition() {
        return spotifyPosition;
    }

    public void setSpotifyPosition(Integer spotifyPosition) {
        this.spotifyPosition = spotifyPosition;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
