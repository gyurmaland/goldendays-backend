package com.goldendays.util;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;

public class SpotifyPlaylistUtil {

    public static String extractSpotifyPlaylistLink(String link) {
        return StringUtils.substringBetween(link, "playlist/", "?si");
    }

    public static String getValuesFromLinkedHashMap(LinkedHashMap linkedHashMap, String substrStart, String substrEnd) {
        String linkedHashMapString = linkedHashMap.toString();
        return StringUtils.substringBetween(linkedHashMapString, substrStart, substrEnd);
    }
}
