package com.goldendays.rest;

import com.goldendays.action.PlaylistAction;
import com.goldendays.payload.SpotifyPlaylistRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/playlist")
public class PlaylistRest {

    @Autowired
    private PlaylistAction playlistAction;

    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<?> getPlaylistFromSpotifyAndSave(@PathVariable Long id) {
        return new ResponseEntity<>(playlistAction.findById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/getPlaylist")
    public ResponseEntity<?> getPlaylistFromSpotifyAndSave(@Valid @RequestBody SpotifyPlaylistRequest spotifyPlaylistRequest) {
        playlistAction.getPlaylistFromSpotifyAndSave(spotifyPlaylistRequest);

        return ResponseEntity.ok("Playlist set!");
    }
}
