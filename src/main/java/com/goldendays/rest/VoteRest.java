package com.goldendays.rest;

import com.goldendays.action.VoteAction;
import com.goldendays.payload.FacebookVoteListRequest;
import com.goldendays.payload.VoteListDecastRequest;
import com.goldendays.payload.VoteListRequest;
import com.goldendays.payload.VotesByUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/vote")
public class VoteRest {

    @Autowired
    VoteAction voteAction;

    @CrossOrigin
    @PostMapping("/castVotes")
    public ResponseEntity<?> castVoteList(@Valid @RequestBody VoteListRequest voteListRequest) {
        voteAction.castVotes(voteListRequest);

        return ResponseEntity.ok("Votes casted!");
    }

    @CrossOrigin
    @PostMapping("/castVotesWithFacebook")
    public ResponseEntity<?> castVoteListWithFacebook(@Valid @RequestBody FacebookVoteListRequest facebookVoteListRequest) {
        voteAction.castVotesWithFacebookAcc(facebookVoteListRequest);

        return ResponseEntity.ok("Votes casted!");
    }

    @CrossOrigin
    @GetMapping("/deCastVotes")
    public ResponseEntity<?> deCastVoteList(@Valid @RequestBody VoteListDecastRequest voteListDecastRequest) {
        voteAction.deCastVotesByUserEmailAndPlaylistId(voteListDecastRequest);

        return ResponseEntity.ok("Votes decasted!");
    }

    @CrossOrigin
    @GetMapping("/allVotesCount/{playlistId}")
    public ResponseEntity<?> countAllVotesByPlaylistId(@PathVariable String playlistId) {
        return ResponseEntity.ok(voteAction.countAllVotesByPlaylistId(Long.parseLong(playlistId)));
    }

    @CrossOrigin
    @GetMapping("/enableVotes/{code}")
    public ResponseEntity<?> enableVotes(@PathVariable String code) {
        voteAction.enableVotes(code);

        return ResponseEntity.ok("Votes enabled!");
    }

    @CrossOrigin
    @PostMapping("/votesByUserAndPlaylist")
    public ResponseEntity<?> getVotesByUserAndPlaylist(@Valid @RequestBody VotesByUserRequest votesByUserRequest) {
        return new ResponseEntity<>(voteAction.findVotesByUserEmailAndPlaylistId(votesByUserRequest.getUserEmail(), votesByUserRequest.getPlaylistId()), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/isUserAlreadyVotedOnThisPlaylist")
    public ResponseEntity<?> isUserAlreadyVotedOnThisPlaylist(@Valid @RequestBody VotesByUserRequest votesByUserRequest) {
        return new ResponseEntity<>(voteAction.isUserAlreadyVotedOnThisPlaylist(votesByUserRequest.getUserEmail(), votesByUserRequest.getPlaylistId()), HttpStatus.OK);
    }
}
