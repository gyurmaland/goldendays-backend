package com.goldendays.rest;

import com.goldendays.action.SongAction;
import com.goldendays.payload.SongListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/song")
public class SongRest {

    @Autowired
    private SongAction songAction;

    @CrossOrigin
    @GetMapping("/getTop10Songs/{playlistId}")
    public ResponseEntity<?> getTop10Songs(@PathVariable String playlistId) {
        return new ResponseEntity<>(songAction.findTop10SongsByVotes(Long.parseLong(playlistId)), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/searchSongs")
    public ResponseEntity<?> searchSongs(@Valid @RequestBody SongListRequest songListRequest) {
        return new ResponseEntity<>(songAction.searchSongs(songListRequest), HttpStatus.OK);
    }
}
