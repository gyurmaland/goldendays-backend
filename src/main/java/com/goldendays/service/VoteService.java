package com.goldendays.service;

import com.goldendays.model.Vote;
import com.goldendays.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VoteService {

    @Autowired
    VoteRepository voteRepository;

    public void castVotes(List<Vote> voteList) {
        voteRepository.saveAll(voteList);
    }

    public void deCastVotesByUserEmailAndPlaylistId(String userEmail, Long playlistId) {
        voteRepository.deleteVoteByUserEmailAndPlaylistId(userEmail, playlistId);
    }

    public int countAllVotesByPlaylistId(Long playlistId) {
        return voteRepository.countAllVotesByPlaylistId(playlistId);
    }

    public void enableVotesWithEmail(String activationCode) { voteRepository.enableVotesWithEmail(activationCode); }

    public List<Vote> findVotesByAppUserId(Long userId) { return voteRepository.findVotesByAppUserId(userId); }

    public List<Vote> findVotesByAppUserEmailAndPlaylistId(String userEmail, Long playlistId) { return voteRepository.findVotesByAppUserEmailAndPlaylistId(userEmail, playlistId); }

    public List<Vote> findVotesByAppUserEmail(String email) { return voteRepository.findVotesByAppUserEmail(email); }

    public List<Vote> findVotesByActivationCode(String activationCode) { return voteRepository.findVotesByActivationCode(activationCode); }
}
