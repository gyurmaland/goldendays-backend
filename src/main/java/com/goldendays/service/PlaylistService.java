package com.goldendays.service;

import com.goldendays.model.Playlist;
import com.goldendays.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaylistService {

    @Autowired
    private PlaylistRepository playlistRepository;

    public Playlist findById(Long id) {
        return playlistRepository.getOne(id);
    }

    public Playlist findBySpotifyId(String spotifyId) {
        return playlistRepository.findBySpotifyId(spotifyId);
    }

    public Playlist savePlaylist(Playlist playlist) {
        return playlistRepository.save(playlist);
    }

    public void updatePlaylistById(String name, String link, String imageUrl, int followers, Long id) {
        playlistRepository.updatePlaylistById(name, link, imageUrl, followers, id);
    }
}
