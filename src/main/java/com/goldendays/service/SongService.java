package com.goldendays.service;

import com.goldendays.model.Song;
import com.goldendays.payload.SongListRequest;
import com.goldendays.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SongService {

    @Autowired
    SongRepository songRepository;

    public Optional<Song> findById(Long id) {
        return songRepository.findById(id);
    }

    public Song findBySpotifyId(String spotifyId) {
        return songRepository.findBySpotifyId(spotifyId);
    }

    public List<Song> findAllByPlaylists_IdOrderBySpotifyPositionAsc(Long playlistId) {
        return songRepository.findAllByPlaylists_IdOrderBySpotifyPositionAsc(playlistId);
    }

    public List<Song> findAllByPlaylists_IdOrderByWeightAscSpotifyPositionAsc(Long playlistId) {
        return songRepository.findAllByPlaylists_IdOrderByWeightAscSpotifyPositionAsc(playlistId);
    }

    public List<Song> findByPlaylists_SpotifyId(String playlistSpotifyId) {
        return songRepository.findByPlaylists_SpotifyId(playlistSpotifyId);
    }

    public List<Song> findByPlaylists_SpotifyIdAndSpotifyId(String playlistSpotifyId, String songSpotifyId) {
        return songRepository.findByPlaylists_SpotifyIdAndSpotifyId(playlistSpotifyId, songSpotifyId);
    }

    public Song findByPlaylists_IdAndAndSpotifyPosition(Long playlistId, Integer spotifyPosition) {
        return songRepository.findByPlaylists_IdAndAndSpotifyPosition(playlistId, spotifyPosition);
    }

    public Page<Song> findTop10SongsByVotes(Long playlistId,Pageable pageable) {
        return songRepository.findAllByPlaylists_IdAndWeightNotOrderByWeightDesc(playlistId, 0, pageable);
    }

    @Transactional
    public void save(Song song) {
        songRepository.save(song);
    }

    @Transactional
    public void deleteBySpotifyId(String spotifyId) {
        songRepository.deleteBySpotifyId(spotifyId);
    }

    @Transactional
    public void saveAll(ArrayList<Song> songList) {
        songRepository.saveAll(songList);
    }

    public Page<Song> searchSongs(SongListRequest songListRequest, PageRequest pageRequest) {
        return songRepository.searchSongs(songListRequest.getPlaylistId(), songListRequest.getPlaylistName(), songListRequest.getSearchStr(), pageRequest);
    }

    @Transactional
    @Modifying
    public void updateSongWeight(Long songId, int weight) {
        songRepository.updateSongWeight(weight, songId);
    }

    @Transactional
    public void updateSpotifyPosition(Long songId, int spotifyPosition) {
        songRepository.updateSpotifyPosition(spotifyPosition, songId);
    }
}
