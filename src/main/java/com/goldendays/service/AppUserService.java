package com.goldendays.service;

import com.goldendays.model.AppUser;
import com.goldendays.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public AppUser findByEmail(String email) {
        return appUserRepository.findByEmail(email);
    }

    public AppUser findByFacebookId(String facebookId) {
        return appUserRepository.findByFacebookId(facebookId);
    }

    public void saveAppUser(AppUser appUser) {
        appUserRepository.save(appUser);
    }
}
